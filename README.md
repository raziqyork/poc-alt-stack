# poc-alt-stack
### Stack (see [Cloud Native Computing Foundation](https://www.cncf.io/))
- `kafka` and `gRPC` and/or `NATS` for interconnect
- `Envoy Proxy` for browser api/live-updates
- `yugabyte` for app db
- `kubernetes` for all non-serverless services (e.g mq, db, etl, cron)
- `reactjs` for ui
- TBD: `aws cognito` or `IdentityServer4` or `ory` for auth
- `ClickHouse` for logging db
- `aws lambda` for serverless (e.g. glue, component logic+handlers)
- TBD: `quartz` for scheduled events
- TBD: `elastic` for seach
- `???` for tenant/environment configuration db
- `prometheus` for metrics/telemetry
- `metabase` for reporting and analytics ui
- `greenplum` for datawarehouse db
- `fluentd` and `fluentbit` for log forwarding
- TBD:`raygun` or `sentry` for capturing js browser errors

### Logging (see [here](https://dzone.com/articles/5-data-storages-for-better-log-management))
- [ClickHouse](https://clickhouse.yandex) docker cluster hosted in AWS EKS 
- TBD: [AWS Timestream](https://aws.amazon.com/timestream/) for aws hosted solution
- TBD: [fluentd](https://www.fluentd.org/) and [fluentbit](https://fluentbit.io/) for log forwarding 
- Single table for all component logs (TBD: also audit logs?)
- TBD: AWS SQS Queue feeding AWS lambda (or docker handler) for logging
- Redundant log messages queued elsewhere? Could be used for audit logging
- UIs - one of [these](https://clickhouse.yandex/docs/en/interfaces/third-party/gui/)
- Clients - C#:[serilog](https://serilog.net/) or NLog, node:[winston or morgan](https://www.loggly.com/blog/node-js-libraries-make-sophisticated-logging-simpler/)
- See [loghouse](https://github.com/flant/loghouse) - full implementation
- JS browser errors should be captured using either commecial offerings like [raygun](https://raygun.com/) and [sentry](https://sentry.io/features/) or just configuring [google analytics](https://developers.google.com/analytics/devguides/collection/analyticsjs/exceptions) to do this.

### Database
- [yugabyte](https://www.yugabyte.com/) document store supporting transactions
- Casandra and PostgreSQL compatable
- IAT env uses yugabyte EKS clusters
- UAT and Prod envs use hosted enterprise YugabyteDB

### Service-Service interconnect
- [kafka](https://kafka.apache.org/) for durable pub-sub (AWS MKS for aws hosting)
- TBD: [gRPC](https://grpc.io/) for service-to-service
- Pluggable auth, tracing, load balancing and health checking
- TBD: [NATS](https://nats.io/documentation/) for pub-sub, queuing and event streaming
- TBD: [natsboard](https://github.com/devfacet/natsboard) for NATS UI
- See [nRPC](https://github.com/nats-rpc/nrpc) for gRPC equivalent over NATS

### Service-Browser interconnect (see [here](https://hackernoon.com/interface-grpc-with-web-using-grpc-web-and-envoy-possibly-the-best-way-forward-3ae9671af67))
- [gRPC-Web](https://github.com/grpc/grpc-web/) hosted in [Envoy Proxy](https://www.envoyproxy.io/)
- Bi-directional streaming with http/2 based transport

### Scheduled events (TBD)
- `quartz` engine written in `dotnet`
- `dotnet` service running in EKS
- uses `yugabyte` for jobs and triggers using PostgreSQL adapter

### UI Framework
- `reactjs`
- TypeScript (tsx)
- `scss`

### UI Gen/CMS (TBD)
- See [staticgen](https://www.staticgen.com) (filter by react)
- [gatsbyjs](https://www.gatsbyjs.org)
- [nextjs](https://www.npmjs.com/package/next)
- See [here](https://geekflare.com/headless-cms/) for hosted options
- hosted: [netlifycms](https://www.netlifycms.org/)
- hosted: [strapi](https://strapi.io/)

### Authentication
- OAuth 2.0 via either [IdentityServer4](https://github.com/IdentityServer) or [ory](https://github.com/ory) (TBD)
- TBD: IdentityServer4 DB: EF via PostgreSQL to `yugabyte`

### Metrics/Telemetry
- [prometheus](https://prometheus.io/) via [cortex](https://github.com/cortexproject/cortex)

### Analytics and Reporting
- [kafka](https://kafka.apache.org/) for ETL (AWS MKS for aws hosting)
- [metabase](https://www.metabase.com/) for reporting and analytics ui (TBC how to integrate in application UI)
- [greenplum](https://greenplum.org/) analytics db (postgresql compatable) (OR [AWS Redshift](https://aws.amazon.com/redshift/) for aws hosting)